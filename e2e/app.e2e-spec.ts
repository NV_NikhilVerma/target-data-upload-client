import { TargetDataUploadPage } from './app.po';

describe('target-data-upload App', () => {
  let page: TargetDataUploadPage;

  beforeEach(() => {
    page = new TargetDataUploadPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
