import { Component, Input, OnInit } from '@angular/core';
import { ScheduleService } from '../_services';
import { MdSnackBar } from '@angular/material';

import { Schedule } from '../_models/schedule';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  countries: Array<{ id: number; name: string }>;
  countryMap: {} = {};
  startDate: Date;
  endDate: Date;
  schedule: Array<Schedule> = [];
  currentSchedule: Array<Schedule> = [];
  lastSchedule: any;
  loading: boolean = false;

  constructor(private scheduleService: ScheduleService, public snackBar: MdSnackBar) {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  ngOnInit() {
    this.loading = true;
    this.scheduleService.getCountries().subscribe(
      data => {
        this.countries = data;
        this.createSchedule();
        this.getLastSchedule();
        this.loading = false;
        console.log(this.countryMap);
        console.log(data);
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  lastUpdatedDate(timestamp) {
    return new Date(timestamp);
  }

  getLastSchedule() {
    this.scheduleService.getLastSchedule().subscribe(
      data => {
        this.lastSchedule = data;
        this.lastSchedule.forEach(scheduleItem => {
          this.schedule[this.countryMap[scheduleItem.country.id]].updatedOn = scheduleItem.updatedOn;
        });
        console.log('last schedule data', data);
      },
      error => {
        console.log('last schedule error', error);
      }
    );
  }

  submitSchedule(index) {
    this.loading = true;
    this.currentSchedule.push(this.schedule[index]);
    console.log(this.currentSchedule);
    this.scheduleService.setSchedule(this.currentSchedule).subscribe(
      data => {
        this.loading = false;
        this.openSnackBar('Schedule set successfully', 'OK');
        console.log(data);
      },
      error => {
        this.loading = false;
        this.openSnackBar('Error in setting schedule', 'OK');
        console.log(error);
      }
    );
  }

  createSchedule() {
    this.countries.forEach((country, index) => {
      this.countryMap[country.id] = index;
      let scheduleItem: Schedule = {
        id: null,
        month: null,
        year: null,
        startDate: null,
        endDate: null,
        createdOn: null,
        updatedOn: null,
        user: {
          id: null,
          country: null
        },
        country: {
          id: country.id,
          name: country.name
        },
        isLatest: null
      };
      this.schedule.push(scheduleItem);
    });
  }
}
