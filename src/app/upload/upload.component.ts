import { Component, OnInit } from "@angular/core";
import { MdSnackBar } from "@angular/material";

import { UploadService } from "../_services";

@Component({
  selector: "app-upload",
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.scss"]
})
export class UploadComponent implements OnInit {
  loading: boolean;
  fileName: string = "No file selected...";
  fileInput: any = null;

  constructor(
    private uploadService: UploadService,
    public snackBar: MdSnackBar
  ) {}

  ngOnInit() {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: -1
    });
  }

  getFile(fileInput) {
    this.fileInput = fileInput;
    if (this.fileInput !== null && this.fileInput.target.files.length > 0) {
      let fileList: FileList = this.fileInput.target.files;
      this.fileName = this.fileInput.target.files[0].name;
    }
  }

  uploadFile() {
    if (this.fileInput !== null && this.fileInput.target.files.length > 0) {
      let fileList: FileList = this.fileInput.target.files;
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("file", file, file.name);
      this.loading = true;
      this.uploadService.uploadFile(formData).subscribe(
        data => {
          this.loading = false;
          this.openSnackBar("Upload successful", "OK");
          this.fileName = "No file selected";
          console.log(data);
        },
        error => {
          let err = error.json().message;
          this.loading = false;
          this.openSnackBar(err, "OK");
          this.fileName = "No file selected";
          console.log(error);
        }
      );
    } else {
      this.openSnackBar("Please select any file...", "OK");
    }
  }

  downloadFile() {
    this.loading = true;
    this.uploadService.downloadFile().subscribe(
      data => {
        this.loading = false;
        this.openSnackBar("File downloaded", "OK");

        var a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        //a.style = "display: none";
        let fileName = "download";
        let url = window.URL.createObjectURL(data);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
        console.log(data);
      },
      error => {
        this.loading = false;
        this.openSnackBar("Error in downloading", "OK");
        console.log(error);
      }
    );
  }
}
