import { Injectable } from "@angular/core";
import {
  Http,
  Response,
  RequestOptions,
  ResponseContentType
} from "@angular/http";
import "rxjs/add/operator/map";

import { AuthenticationService } from "./authentication.service";
import { ConstantService } from "./constant.service";

@Injectable()
export class UploadService {
  constructor(
    private http: Http,
    private authService: AuthenticationService,
    private constant: ConstantService
  ) {}

  uploadFile(formData) {
    let requestOptions = new RequestOptions();
    let headers = {
      //'Content-Type': 'multipart/form-data; boundary=HereGoes',
      //'Accept': 'application/json'
    };
    //requestOptions = this.authService.jwt(headers);
    return this.http
      .post(this.constant.REST_END_POINT + "upload/data", formData)
      .map((response: Response) => {
        return response.json();
      });
  }

  downloadFile() {
    let requestOptions = new RequestOptions();
    let headers = {
      "Content-Type": "application/json"
      //Accept: "application/vnd.ms-excel"
      //responseType: ResponseContentType.Blob
    };

    requestOptions = this.authService.jwt(headers);
    requestOptions.responseType = ResponseContentType.Blob;
    return this.http
      .post(
        this.constant.REST_END_POINT + "download/latest",
        { id: 1, country: { id: 1, name: "Vietnam" } },
        requestOptions
      )
      .map((response: Response) => {
        console.log(response);
        //return response.json();
        //console.log(new Blob([response._body], {type: 'application/vnd.ms-excel'}));
        return new Blob([response["_body"]], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"
        });
      });
  }
}
