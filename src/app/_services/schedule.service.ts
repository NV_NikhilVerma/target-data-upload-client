import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';
import { ConstantService } from './constant.service';

@Injectable()
export class ScheduleService {

  constructor(private http: Http, private authService: AuthenticationService, private constant: ConstantService) { }

  getCountries() {
   let requestOptions = new RequestOptions;
   let headers = {
    //'Content-Type': 'multipart/form-data; boundary=HereGoes',
    //'Accept': 'application/json'
   };
   //requestOptions = this.authService.jwt(headers);
   return this.http.get(this.constant.REST_END_POINT + 'schedule/countries')
     .map((response: Response) => {
         return response.json();
     });
  }

  getLastSchedule() {
    let requestOptions = new RequestOptions;
    let headers = {
     //'Content-Type': 'multipart/form-data; boundary=HereGoes',
     //'Accept': 'application/json'
    };
    //requestOptions = this.authService.jwt(headers);
    return this.http.get(this.constant.REST_END_POINT + 'schedule/lastupdate')
      .map((response: Response) => {
          return response.json();
      });
  }

  setSchedule(data) {
    let requestOptions = new RequestOptions;
    let headers = {
     //'Content-Type': 'multipart/form-data; boundary=HereGoes',
     //'Accept': 'application/json'
    };
    //requestOptions = this.authService.jwt(headers);
    return this.http.post(this.constant.REST_END_POINT + 'schedule/create', data)
      .map((response: Response) => {
          return response.json();
      });
  }

}
