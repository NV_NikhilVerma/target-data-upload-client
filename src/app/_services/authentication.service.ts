import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }

  jwt(customHeaders) {
    let headers = new Headers();
      // create authorization header with jwt token
      for(var header in customHeaders) {
        headers.append(header, customHeaders[header]);
       }
       return new RequestOptions({ headers: headers });      
    //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //   if (currentUser.access_token) {
    //       let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.access_token });
    //       for(var header in customHeaders) {
    //        headers.append(header, customHeaders[header]);
    //       }
    //       return new RequestOptions({ headers: headers });
    //   }
  }

}
