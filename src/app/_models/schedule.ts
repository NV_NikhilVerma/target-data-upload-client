export class Schedule {
  id: number;
  month: number;
  year: number;
  startDate: Date;
  endDate: Date;
  createdOn: Date;
  updatedOn: Date;
  user: {
    id: number,
    country: string
  };
  country: {
    id: number;
    name: string;
  };
  isLatest: boolean;
}
