import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UploadComponent } from './upload/upload.component';
import { ScheduleComponent } from './schedule/schedule.component';
//import { AuthGuard } from './_guards/auth.guard';


const routes: Routes = [
  {path: '', component: HomeComponent, /*canActivate: [AuthGuard]*/ children: [
    {path: 'upload', component: UploadComponent},
    {path: 'schedule', component: ScheduleComponent},
    {path: '', redirectTo: 'upload', pathMatch: 'full'}
  ]},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
